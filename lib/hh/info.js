import * as cheerio from "cheerio";

//TODO: better name search and fix images

export async function searchH(name) {
  const result = await fetch(`https://hentaihaven.xxx/?s=${name}`);
  const htmlPage = await result.text();

  const $ = cheerio.load(htmlPage);

  const results = [];
  $(".row.c-tabs-item__content").each((i, el) => {
    const link = $(el).find(".post-title a");
    results.push({
      title: link.text(),
      url: link.attr("href"),
    });
  });

  if (results.length === 0) return [];

  const episodes = await getHEpisodes(results[0].url);

  return [
    {
      map: true,
      providerId: "hentai haven",
      episodes: episodes.map((e) => ({ ...e, id: name + "-" + e.name })),
    },
  ];
}

export async function getHEpisodes(animeUrl) {
  const result = await fetch(animeUrl);
  const htmlPage = await result.text();

  const $ = cheerio.load(htmlPage);
  const episodes = $(".wp-manga-chapter").toArray();

  const links = [];

  for (let i = 0; i < episodes.length; i++) {
    const a = $(episodes[i]).find("a");
    const img = $(episodes[i]).find("img");
    const number = a.text().match(/\d/)[0];
    links.push({
      name: a.text().trim(),
      url: a.attr("href"),
      image: img.attr("src"),
      number: number ? parseInt(number) : i,
    });
  }

  links.sort((a, b) => {
    return a.name.localeCompare(b.name); // Sort alphabetically
  });
  return links;
}

export async function getHEpisodeSource(episodeUrl) {
  const result = await fetch(episodeUrl);
  const htmlPage = await result.text();

  const $episode = cheerio.load(htmlPage);
  const iframe = $episode(".player_logic_item iframe");

  const resultLink = await fetch(iframe.attr("src"));
  const htmlPageLink = await resultLink.text();

  const $ = cheerio.load(htmlPageLink);

  const script = $('script:not([src])[type$="text/javascript"]').text();
  const enValue = script.match(/var en = '([^']+)'/)[1];
  const ivValue = script.match(/var iv = '([^']+)'/)[1];
  const uriValue = script.match(/var uri = '([^']+)'/)[1];

  const data = new FormData();
  data.append("action", "zarat_get_data_player_ajax");
  data.append("a", enValue);
  data.append("b", ivValue);
  let init = {
    method: "POST",
    mode: "cors",
    body: data,
    cache: "default",
  };
  const eResult = await fetch(`${uriValue}api.php`, init);
  const eData = await eResult.json();

  return eData;
}
