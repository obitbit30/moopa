import React from "react";
import { getFormat } from "../../../../utils/getFormat";

export default function InfoChip({ info, color, className }) {
  return (
    <>
      <div
        className={`flex-wrap w-full justify-start md:pt-1 gap-x-4 gap-y-2 ${className}`}
      >
        {info?.episodes && (
          <div
            className={`dynamic-text rounded-md px-2 font-karla font-bold`}
            style={color}
          >
            {info?.episodes} Episodes
          </div>
        )}
        {info?.averageScore && (
          <div
            className={`dynamic-text rounded-md px-2 font-karla font-bold`}
            style={color}
          >
            {info?.averageScore}%
          </div>
        )}
        {info?.format && (
          <div
            className={`dynamic-text rounded-md px-2 font-karla font-bold`}
            style={color}
          >
            {getFormat(info?.format)}
          </div>
        )}
        {info?.status && (
          <div
            className={`dynamic-text rounded-md px-2 font-karla font-bold`}
            style={color}
          >
            {info?.status}
          </div>
        )}
        {info?.genres &&
          info.genres.map((g) => (
            <div
              key={g}
              className={`dynamic-text rounded-md px-2 font-karla font-bold bg-sky-400 text-black`}
            >
              {g}
            </div>
          ))}
      </div>
      <div
        className={`flex mt-1 gap-2 overflow-x-scroll whitespace-nowrap w-full md:w-[500px] lg:w-[1000px] scrollbar-none ${className}`}
      >
        {info?.tags &&
          info.tags.map((t) => (
            <div
              key={t.name}
              className={`dynamic-text rounded-md px-2 font-karla font-bold bg-white text-sm text-black`}
            >
              {t.name}
            </div>
          ))}
      </div>
    </>
  );
}
